package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
  public Celsius(float t) {
	  super(t);
  }
  
  public String toString() {
	  return "Celsius is " + this.getValue();
  }

@Override
public Temperature toCelsius() {
	return this;
}

@Override
public Temperature toFahrenheit() {
	float f = (9*this.getValue())/5 + 32;
	Temperature t = new Celsius(f);
	return t;
	}
}
