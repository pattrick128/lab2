package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
  public Fahrenheit(float t) {
	  super(t);
  }
  
  public String toString() {
	  return "Fahrenheit is " + this.getValue();
  }

@Override
public Temperature toCelsius() {
	float c = (5*(this.getValue()-32))/9;
	Temperature t = new Fahrenheit(c);
	return t;
}

@Override
public Temperature toFahrenheit() {
	return this;
}
}
